#!/usr/bin/env bash

backup_db() {
    local termdir=${ARCHIVEDIR}/${TERM}

    echo "[BACKUP-${NAME}] Backing up database into '${termdir}'"
    mkdir -p ${termdir}
    pg_dump ${NAME} | gzip > ${termdir}/${NAME}.${TERM}.psql.gz
    if [[ ${SQLINSTANCES} == *${NAME},* ]]; then
        echo "[BACKUP-${NAME}] Backing up SQL-instance database into '${termdir}'"
        pg_dump ${NAME}_data | gzip > ${termdir}/${NAME}_data.${TERM}.psql.gz
    fi
}

# script starts here
if [[ $# -ne 2 ]]; then
	echo "Usage: $0 instance_name term"
	exit 1
fi

# vars
THISSCRIPT=$(readlink -f ${BASH_SOURCE})
THISSCRIPTDIR=$(dirname ${THISSCRIPT})
. ${THISSCRIPTDIR}/config.sh
NAME=$1
TERM=$2

# main
backup_db
