#!/usr/bin/env bash

APACHECONFFILE=/etc/apache2/conf-enabled/httpd.conf
POSTGRESCONFFILE=/etc/postgresql/9.3/main/pg_hba.conf
HOMEDIR=/home/pcrsadmin
ARCHIVEDIR=${HOMEDIR}/archive
BACKUPDIR=${HOMEDIR}/backup
BINDIR=${HOMEDIR}/bin
USERSDIR=${HOMEDIR}/classlists
LOGSDIR=${HOMEDIR}/logs
WWWDIR=${HOMEDIR}/www
DBLISTFILE=${HOMEDIR}/.pgpass
PCRSBRANCH=productionv1.0
SQLINSTANCES="343,343prep,"
CINSTANCES="209,"
JAVAINSTANCES="207,"
