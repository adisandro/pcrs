#!/usr/bin/env bash

enable_crontab() {
    local usersfile=${USERSDIR}/${USERS}

    echo "[ENABLE-${NAME}] Adding crontab entry to load students from '${usersfile}'"
    echo "[ENABLE-${NAME}] (load students at '1h${MINUTES}m' and '13h${MINUTES}m', clear sessions at '1h${MINUTES2}m')"
    touch ${usersfile}.old
    export BINDIR NAME USERSDIR USERS PCRSDIR DIR SETTINGSDIR MINUTES MINUTES2 PROCESSES SQLSNIPPET
    SQLSNIPPET=
    if [[ ${SQLINSTANCES} == *${NAME},* ]]; then
        DATANAME=${NAME}_data
        export DATANAME
        SQLSNIPPET=$(envsubst < ${SNIPPETSDIR}/crontab_sql)
    fi
    SNIPPET=$(envsubst < ${SNIPPETSDIR}/crontab)
    (crontab -l && echo "${SNIPPET}") | crontab -
}

enable_apache() {
    echo "[ENABLE-${NAME}] Adding apache conf entry"
    SNIPPET=$(envsubst < ${SNIPPETSDIR}/apache)
    if (echo "${SNIPPET}" | sudo tee -a ${APACHECONFFILE} > /dev/null); then
        sudo service apache2 restart
    else
        echo "[ENABLE-${NAME}] Can't sudo: ask an admin to add the following to '${APACHECONFFILE}' and restart apache"
        read -p "${SNIPPET}"
    fi
}

# script starts here
if [[ $# -lt 3 || $# -gt 4 ]]; then
	echo "Usage: $0 instance_name users_file crontab_minutes [apache_processes]"
	exit 1
fi

# vars
THISSCRIPT=$(readlink -f ${BASH_SOURCE})
THISSCRIPTDIR=$(dirname ${THISSCRIPT})
. ${THISSCRIPTDIR}/config.sh
NAME=$1
USERS=$2
MINUTES=$3
if [[ $# -eq 4 ]]; then
    PROCESSES=$4
else
    PROCESSES=5
fi
MINUTES2=$((MINUTES + 1))
DIR=${WWWDIR}/${NAME}
PCRSDIR=${DIR}/pcrs
SETTINGSDIR=${PCRSDIR}/pcrs
SNIPPETSDIR=${THISSCRIPTDIR}/snippets

# main
enable_crontab
enable_apache
