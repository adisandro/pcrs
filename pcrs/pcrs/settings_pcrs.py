# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Don't touch this file, create/edit settings_local.py with what you need to change.
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
from datetime import timedelta

# Select the types of problems visible in the UI.
# app_name : language name
INSTALLED_PROBLEM_APPS = {
    # 'problems_python': 'Python',
    # 'problems_c': 'C',
    # 'problems_java': 'Java',
    # 'problems_rdb': '',
    # 'problems_sql': 'SQL',
    # 'problems_ra': 'Relational Algebra',
    # 'problems_r': 'R',
    # 'problems_multiple_choice': '',
    # 'problems_rating': '',
    # 'problems_short_answer': '',
}

PROBLEM_APPS = (
    'problems_python',
    'problems_c',
    'problems_java',
    'problems_sql',
    'problems_rdb',
    'problems_ra',
    'problems_r',
    'problems_multiple_choice',
    'problems_rating',
    'problems_short_answer',
)


CRON_CLASSES = {
    'problems_r.cron.FileCronJob'
}

# Time, in seconds, of how long each mastery quiz lasts. Custom student modifiers will change this.
# Default: 20 minutes (1200 seconds)
MASTERY_QUIZ_DURATION = 1200

# Time, in seconds, of how long each student has to join a mastery quiz, before it is expired.
# Default: 10 minutes (600 seconds)
MASTERY_QUIZ_EXPIRY_TIME = 600

# Mastery quiz number of attempts for code questions
MASTERY_QUIZ_CODE_ATTEMPTS = 3
# Mastery quiz number of attempts for multiple choice questions
MASTERY_QUIZ_MC_ATTEMPTS = 1

# Challenge graph format or vanilla PCRS
CHALLENGE_GRAPH_ENABLED = True
