from users.views_mixins import InstructorViewMixin, CourseStaffViewMixin
from django.views.generic import TemplateView
from users.models import PCRSUser, PCRSUserManager
from content.models import Quest
from analytics.analytics_helper import QuestAnalyticsHelper, QuestAnalyticsManager
from users.section_views import SectionViewMixin
from django import forms
from crispy_forms.helper import FormHelper
from django.http import HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
from io import TextIOWrapper, StringIO
from mastery.models import MasteryQuizTestSubmission, MasteryQuizTest, MasteryQuizChallenge
from django.core.urlresolvers import reverse
from django.contrib import messages
import datetime
import csv
import codecs

class AdminMasteryQuizImportExport(CourseStaffViewMixin, SectionViewMixin, TemplateView):
	''' This class is responsible for importing and exporting mastery quiz progress. '''

	template_name = 'content/admin_mastery_quiz_manager.html'

	def get_context_data(self, **kwargs):
		context = {}
		context['page_title'] = 'Mastery Quiz Manager'
		context['mastery_quizzes'] = MasteryQuizChallenge.objects.all()
		context['file_upload'] = AdminMasteryQuizManagerForm()
		return context

	def post(self, request, *args, **kwargs):
		''' This function is responsible for parsing a CSV file and importing the mastery quiz data
		into our database. '''
		file_upload_form = AdminMasteryQuizManagerForm(request.POST, request.FILES)
		context = self.get_context_data()
		context['form'] = file_upload_form
		if file_upload_form.is_valid():
			context['success'] = 'Successfully uploaded file.'
			# Because CSV parsing doesn't operating on bytes, only strings
			data_encoded = TextIOWrapper(request.FILES['file'].file, encoding=request.encoding)
			csv_file = csv.DictReader(data_encoded)
			# For each entry, save
			row_count = 0
			for row in csv_file:
				row_count += 1
				submission = MasteryQuizTestSubmission(
					user_id=row['user_id'],
					mastery_quiz_test_id=row['mastery_quiz_test_id'],
					score=row['score'],
					date_marked=row['date_marked']
				).save()
			messages.success(request, 'Successfully imported ' + str(row_count) + ' rows of data.')
		else:
			messages.error(request, 'Failed to import data.')
		return HttpResponseRedirect(reverse('mastery_quiz_manager'))

	def get_mastery_quiz_data(self):
		''' This function is responsible for returning an HttpResponse containing a CSV File of all of the
		mastery quiz data. '''
		# Use temporary in-memory storage since we're not saving the file
		output = StringIO()
		writer = csv.DictWriter(output, fieldnames=['user_id', 'mastery_quiz_test_id', 'out_of', 'score', 'date_marked'])
		writer.writeheader()
		submissions = MasteryQuizTestSubmission.objects.all()
		# Write all of the rows into our CSV file
		for submission in submissions:
			submission_test = MasteryQuizTest.objects.filter(id=submission.mastery_quiz_test_id).get()
			writer.writerow({
				'user_id': submission.user_id,
				'mastery_quiz_test_id': submission.mastery_quiz_test_id,
				'out_of': submission_test.out_of,
				'score': submission.score,
				'date_marked': submission.date_marked
			})
		# Set the appropriate HTTP Headers and response
		response = HttpResponse(output.getvalue(), content_type='text/csv')
		response['Content-Disposition'] = 'attachment; filename="mastery_quiz_dump_{}.csv"'.format(
			datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
		return response

class AdminMasteryQuizManagerForm(forms.Form):
	''' This class is responsible for generating the admin mastery quiz upload form. '''

	# Our form just has an upload field
	file = forms.FileField()

	def __init__(self, *args, **kwargs):
		super(AdminMasteryQuizManagerForm, self).__init__(*args, **kwargs)
		# Change the style and id attributes of the file field
		self.fields['file'].widget.attrs['style'] = 'display: none;'
