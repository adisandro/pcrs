from django.conf import settings
from django.conf.urls import url
from mastery.views import MasteryTAView, MasteryQuizAPI, \
	StudentMasteryQuizAPI, StudentMasteryQuiz, MasteryCreationView, MasteryEditAPI, MasteryQuizTimes

urlpatterns = [
	# TA Template Views
    url(r'^setup$', MasteryTAView.ta_view, name='mastery_ta'),

    # Student Template Views
    url(r'^quiz$', StudentMasteryQuiz.quiz_view, name='quiz'),
    url(r'^quiz_result$', StudentMasteryQuiz.latest_quiz_result_view, name='latest_quiz_result'),

    # TA API Views
    url(r'^setup/api/start_session$', MasteryQuizAPI.start_session),
    url(r'^setup/api/stop_session$', MasteryQuizAPI.stop_session),

    # Student API Views
    url(r'^student/api/join_session$', StudentMasteryQuizAPI.join_session),

    # Admin Mastery Quiz Edit Views
    url(r'^admin/edit_quizzes$', MasteryCreationView.edit_mastery_quizzes),
    url(r'^admin/edit_quizzes/test/add$', MasteryEditAPI.save_mastery_test),
    url(r'^admin/edit_quizzes/test/delete$', MasteryEditAPI.delete_mastery_test),
    
    # Admin Mastery Quiz Time Edit
    url(r'^admin/mastery_quiz_times$', MasteryQuizTimes.template_view),
    url(r'^admin/mastery_quiz_times/edit$', MasteryQuizTimes.add_edit_custom_time),

    # Admin Mastery Quiz Add Quiz Challenge
    url(r'^admin/add_mastery_quiz$', MasteryEditAPI.add_quiz_challenge),
    url(r'^admin/delete_mastery_quiz$', MasteryEditAPI.delete_quiz_challenge),
    url(r'^admin/save_mastery_quiz$', MasteryEditAPI.save_quiz_challenge),
    url(r'^admin/get_challenges$', MasteryEditAPI.get_challenges)
    
]